from google.appengine.ext.webapp import template
from models import AppUser

import json as json
import os
import main

class htmlRequest(object) :
	def __init__(self, template):
		self.template = template
		
	def __call__(self, func):
		def wrap(request, *a, **kw):
			request.user = AppUser.TokenUser(request.request.get("token"))
			request.response.headers["Content-Type"] = "text/html"
			template_values = func(request, *a, **kw)
			path = os.path.join(os.path.dirname(main.__file__), 'templates/' + self.template)
			return str(template.render(path, template_values))
		
		return wrap
		
class jsonRequest(object):
	def __init__(self):
		pass
	def __call__(self,func):
		def wrap(request,*a,**kw):
			request.user = AppUser.TokenUser(request.request.get("token"))
			request.response.headers["Content-Type"] = "application/json"
			return json.dumps( func(request,*a,**kw) )
		return wrap