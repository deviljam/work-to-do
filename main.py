from google.appengine.ext.webapp import template
from google.appengine.ext import webapp
from google.appengine.api import users
from google.appengine.ext import db
from google.appengine.ext.webapp.util import login_required
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.api import mail

from webapp2_extras import sessions

import wsgiref.handlers
import logging
import re

import models
from controllers import pages, api

class MainHandler(webapp.RequestHandler):
	#out = ""
	route = False
	
	def get(self, route):
		self.method = "GET"
		self.action(route)
		
	def post(self, route):
		self.method = "POST"
		self.action(route)
			
	def action(self, route):
		self.route = route
		
		sessions.default_config["secret_key"] = "tui=dafpadojg"
		self.session = sessions.get_store(request=self.request)
	
		if self.call("^/$", pages.home):return
		if self.call("^/\d+/?$", pages.home):return
		if self.call("^/report/?$", pages.report):return
		
		if self.call("^/api/projects/?$", api.projects):return
		if self.call("^/api/task/(\d+)/?$", api.task_get):return		
		if self.call("^/api/task/create/?$", api.task_create):return		
		if self.call("^/api/task/edit/(\d+)/?$", api.task_edit):return
		if self.call("^/api/task/delete/(\d+)/?$", api.task_delete):return
		if self.call("^/api/task/status/(\d+)/?$", api.task_status):return
		if self.call("^/api/task/report/?$", api.task_report):return
		
		if self.call("^/api/task/update_all/?$", api.update_all):return
		
		if self.call("^/api/user/login/?$", api.user_login):return
		if self.call("^/api/user/create/?$", api.user_create):return
		
		return "404"
	
	def call(self, reg, function):
		m = re.match(reg,self.route)
		
		if m :
			groups = m.groups()
			#self.out += function(self.request, *groups)
			self.response.out.write(function(self, *groups))
			return True
		else :
			return False
	
def app():
	try:
		#return MainHandler().action( request )
		application = webapp.WSGIApplication([('(/.*/?)', MainHandler),], debug=True)
		run_wsgi_app(application)
	except Exception, e:
		raise Exception( "THERE WAS A PROBLEM: " + str ( e ) )
		logging.error ( e )

if __name__ == "__main__":		
	app()