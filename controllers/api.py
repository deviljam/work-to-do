from controllers import jsonRequest
from models import Task,AppUser,Report
import re
import datetime

# TASKS

@jsonRequest()
def projects(request):
	out = []
	projects = Task.all().filter("root",None)
	for p in projects:
		out.append( p.todict() )
	
	return out
	
@jsonRequest()
def task_get(request,id):	
	out = []
	task = Task.get_by_id(easyInt(id))
	if task:
		return task.todict()
	return {}
	
@jsonRequest()
def task_create(request):	
	out = []
	#import pdb; pdb.set_trace()
	if request.post and request.user:
		task = Task()
		task.title = request.request.get("title")
		task.time = easyTime(request.request.get("time"))
		task.type = 0
		task.description = request.request.get("description")
		task.creator = request.user
		if easyInt(request.request.get("parent")) > 0:
			parent = Task.get_by_id(easyInt(request.request.get("parent")))
			if not parent == None:
				task.root = parent
				task.type = easyInt(request.request.get("type"))
		task.save()
		
		return task.todict()
	raise Exception("could not complete task creation")
	
@jsonRequest()
def task_edit(request,id):
	out = []
	
	task = Task.get_by_id(easyInt(id))
	if request.post and request.user and task:
		task.title = request.request.get("title")
		task.time = easyTime(request.request.get("time"))
		task.description = request.request.get("description")
		if task.root:
			task.type = easyInt(request.request.get("type"))
		task.save()
		return task.todict()
		
	raise Exception("could not complete edit task")
	
@jsonRequest()
def task_delete(request,id):
	out = {}
	task = Task.get_by_id(easyInt(id))
	if request.post and request.user and task:
		if task.root:
			out["parent_id"] = easyInt(task.root.key().id())
		Task.delete(task)
		return out
	raise Exception("could not delete task")

@jsonRequest()
def task_status(request,id):
	out = []
	
	task = Task.get_by_id(easyInt(id))
	if request.post and request.user and task:
		original_status = task.status
		task.status = request.request.get("status")
		task.save()
		
		#Create report of this event
		if task.status == "complete":
			Report.log(
				task,
				request.user,
				"Task status changed from %s to %s" % (original_status,task.status)
			)
		return task.todict()
		
	raise Exception("could not update task status")
	
@jsonRequest()
def task_report(request):
	out = {"tasks":[]}
	if request.post and request.user:
		date = easyDatetime(request.request.get("from"))
		tasks = Task.all().filter("modified >", date).order('modified')
		for task in tasks:
			reports = Report.all().filter("task",task).filter('created >',date).order('created')
			if reports.count() > 0:
				task_out = task.todict(show_subtasks=False)
				task_out["reports"] = []
				for report in reports:
					task_out["reports"].append( report.todict() )
				out["tasks"].append(task_out)
	return out
	
# USERS

@jsonRequest()
def user_login(request):
	#create admin if none exists
	if AppUser.all().filter("name","admin").count() < 1:
		AppUser.create("admin","4b6579");

	out = {}
	if request.post:
		user = AppUser.login(request.request.get("name"), request.request.get("password"))
		if user:
			request.session.save_sessions(user.todict())
			out = user.todict(get_token=True)
	return out
	
@jsonRequest()
def user_create(request):
	out = []
	if request.post:
		user = AppUser.create(
			request.request.get("name"),
			request.request.get("password")
		)
		if user:
			return user.todict()
			
	return {"message":"could not complete user creation"}
	
@jsonRequest()
def update_all(request):
	tasks = Task.all()
	for task in tasks:
		task.save()
	return {"message":"%s tasks updated" % tasks.count()}
	
@jsonRequest()
def user_edit(request,id):
	out = []
	if request.post:
		user = AppUser.get_by_id(easyInt(id))
		if user:
			return user.todict()
	return {"message":"could not edit user"}
	

def easyInt(x):
	try:
		return int(float(str(x)))
	except:
		return 0

def easyTime(x):
	x = x.strip()
	time = 0
	if re.search("(\d+)d",x):
		time += easyInt( re.search("(\d+)d",x).groups(0)[0] ) * 60 * 8
	if re.search("(\d+)h",x):
		time += easyInt( re.search("(\d+)h",x).groups(0)[0] ) * 60
	if re.search("(\d+)m",x):
		time += easyInt( re.search("(\d+)m",x).groups(0)[0] )
	return time

def easyDatetime(x):
	try:
		reg = "(\d{4})-(\d\d)-(\d\d)"
		if re.search(reg,x):
			groups = re.search(reg,x).groups(0)
			return datetime.datetime(
				year=easyInt(groups[0]),
				month=easyInt(groups[1]),
				day=easyInt(groups[2])
			)
		raise Exception("String did not match date format")
	except Exception:
		return datetime.datetime.now()