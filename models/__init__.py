from google.appengine.api import taskqueue
from google.appengine.ext import db
from google.appengine.api.datastore_types import GeoPt
from google.appengine.api.datastore_types import Email
from google.appengine.api.datastore_types import Text
from operator import itemgetter, attrgetter

import logging
import json
import math
import re
import md5
import datetime as datetime

class AppModel( db.Model):
	created = db.DateTimeProperty()
	modified = db.DateTimeProperty()
	def __init__(self,*a,**kw):
		super(AppModel,self).__init__(*a,**kw)
		self.created = datetime.datetime.now()
		self.modified = datetime.datetime.now()
		
	def save(self,*a,**kw):
		modified = datetime.datetime.now()
		super(AppModel, self).save(*a,**kw)
		
	def todict(self):
		return {}

class AppUser(AppModel):
	name = db.StringProperty()
	password = db.StringProperty()
	token = db.StringProperty()
	
	@staticmethod
	def login(username,password):
		password = md5.new(password).hexdigest()
		users = AppUser.all().filter("name",username).filter("password",password)
		for user in users:
			return user
		return None
		
	@staticmethod
	def TokenUser(token):
		users = AppUser.all().filter("token",token)
		for user in users:
			return user
		return None
		
	@staticmethod
	def create(username,password):
		user = AppUser(name=username)
		user.password = md5.new(password).hexdigest()
		user.save()
		return user
	
	def getToken(self,):
		token = md5.new(self.name+self.password+datetime.datetime.now().strftime("%Y%m%d%H%M%S")+"salt").hexdigest()
		token = str(self.key().id()) +"_"+ token
		self.token = token
		self.save()
		return token
			
	def todict(self,get_token=False):
		#import pdb; pdb.set_trace()
		out = {
			"id" : self.key().id(),
			"name" : self.name
		}
		if get_token:
			out["token"] = self.getToken()
			
		return out		
		

class Task(AppModel):
	TASKTYPE_PROJECT = 0
	TASKTYPE_EPIC = 1
	TASKTYPE_FEATURE = 2
	TASKTYPE_TASK = 3
	TASKTYPE_BUG = 4
	TASKTYPE_COMMENT = 5

	title = db.StringProperty()
	status = db.StringProperty(default="todo")
	description = db.TextProperty()
	type = db.IntegerProperty(default=0)
	time = db.IntegerProperty(default=0)
	remaining = db.IntegerProperty(default=0)
	creator = db.ReferenceProperty(AppUser)
	assignees = db.ListProperty(int)
	root = db.SelfReferenceProperty(default=None)
	
	def save(self):
		super(Task,self).save()
		
		subtasks = Task.all().filter("root",self)
		if subtasks.count() > 0:
			self.time = 0
			self.remaining = 0
			for task in subtasks:
				self.time += task.time
				self.remaining += task.remaining
			super(Task,self).save()
		else:
			if self.status == "complete" or self.status == "pendingreview":
				self.remaining = 0
			else:
				self.remaining = self.time
			super(Task,self).save()
		
		if self.root:
			self.root.save()

	@staticmethod
	def delete(task):
		parent = task.root
		subtasks = Task.all().filter("root",task)
		for subtask in subtasks:
			Task.delete(subtask)
		db.delete(task)
		if parent:
			parent.save()
	
	def todict(self,show_subtasks=True):
		assignees = []
		for user in self.assignees:
			assignees.append( user.todict() )
			
		out = {
			"id" : self.key().id(),
			"title" : self.title,
			"status" : self.status,
			"type" : self.type,
			"description" : self.description,
			"time" : self.time,
			"remaining" : self.remaining,
			"creator" : self.creator.todict(),
			"assignees" : assignees,
			"subtasks" : [],
			"roots" : []
		}
		if show_subtasks:
			subtasks = Task.all().filter("root",self).order(status)
			for task in subtasks:
				out["subtasks"].append( task.todict(show_subtasks=False) )
				
			parent = self.root
			while parent:
				out["roots"].append( parent.todict(show_subtasks=False) )
				parent = parent.root
				
		return out

class Report(AppModel):
	description = db.TextProperty()
	user = db.ReferenceProperty(AppUser)
	task = db.ReferenceProperty(Task)
	
	@staticmethod
	def log(task,user,description):
		try:
			report = Report(task=task,user=user,description=description)
			report.save()
		except Exception:
			pass
	
	def todict(self):
		return {
			"description" : self.description,
			"user" : self.user.todict(),
			"task" : self.task.todict(show_subtasks=False),
			"created" : self.created.strftime("%Y-%m-%d %H:%M:%S")
		}