$(document).ready(function(){
	App.init();
});

App = {
	"userToken" : false,
	"currentId" : false,
	"events" : {},
	"on" : function(name,callback){
		if ( !(name in this.events)){
			this.events[name] = new Array();
		}
		this.events[name].push(callback);
	
	},
	"trigger" : function(name){
		if(name in this.events){
			for(var i=0; i<this.events[name].length; i++){
				var args = Array.prototype.slice.call(arguments, 1);
				this.events[name][i].apply(this,args);
			}
		}
	},
	"init" : function(){
		var self = this;
		
		$('#userPanel_login_submit').on('click', function(ev){
			ev.preventDefault();
			self.headerLogin();
		});
		
		this.on('login', function(res){
			if( res.token ){
				self.userToken = res.token;
				$('#userPanel .login').fadeOut(300);
				$('#userPanel .panel').delay(300).fadeIn(300);
				$('.userNameGoesHere').html(res.name);
			}			
		});
		
		// Routing
		if ( location.pathname.match('^/report$') ) {
			this._init_report();
		} else if ( location.pathname.match('^/\\d+$') ) {
			this._init_home();
			this.getTask(location.pathname.match('^/(\\d+)$')[1]);
		} else {
			this._init_home();
		}
		
		window.onpopstate = function(event){
			if ( location.pathname.match('^/\\d+$') ) {
				App.getTask(location.pathname.match('^/(\\d+)$')[1]);
			} else {
				App.goHome()
			}
		};
	},
	
	"_init_home" : function(){
		var self = this;
		
		$('#user-create-submit').on('click', function(ev){
			ev.preventDefault();
			self.createUser();
		});
		
		$('#task-create-submit').on('click', function(ev){
			ev.preventDefault();
			self.createTask();
		});		
		
		$('#task-details-edit-save').on('click', function(ev){
			ev.preventDefault();
			self.editTask();
		});
		
		
		
		$('#task-details-edit').on('click', function(ev){
			ev.preventDefault();
			$('#task-details .display').hide();
			$('#task-details .edit').show();
		});

		$('#task-details-delete').on('click', function(ev){
			ev.preventDefault();
			if ( confirm("Are you sure you wish to delete this task?")){
				var id = $('#task-details [name=id]').val()
				self.deleteTask(id);
			}
		});
		
		$('#task-details-status-select').on('change',function(ev){
			id = $('#task-details [name=id]').val()
			self.updateTaskStatus( id, this.value );
		});
		
		this.on('login', function(res){
			this.goHome();
		});
		
		this.on('task.loaded',function(task){
			self.renderTask(task);
		});
	},
	
	"_init_report" : function(){
		var self = this;
		
		$('#fetch-report').on('click',function(){
			var data = {
				'token' : App.userToken,
				'from' : $('input[name=from]').val()
			};
			$.ajax({
				'url' : '/api/task/report',
				'type' : 'post',
				'data' : data,
				'success' : function(res){
					self.renderReports(res);
				}
			});
		});
	},
	
	goHome : function(){
		var self = this;
		self.clearTask();
		
		$.ajax({
			"url" : "/api/projects",
			"success":function(res){
				wrapper = {
					"id" : false,
					"subtasks" : res
				}
				self.renderTask( wrapper );
				window.setPath("/","home");
			}
		});
	},
	
	clearTask : function(){
		$('#current-task').hide();
	},
	refreshTask : function(){
		if(this.currentId){
			this.getTask(this.currentId);
		}
	},
	renderTask : function( data ) {
		var self = this;
		
		if( data.id ){
			//Has task data
			this.currentId = data.id;
			
			//Clear path/breadcrumbs
			$("#task-details .roots").html("");
			
			//Create crumb for home
			var $el = $('<li>Home</li>');
			$el.on('click',function(ev){	
				ev.preventDefault();
				self.goHome();
			});
			$("#task-details .roots").append($el);
			
			//Create all other crumbs
			//for( var i=0; i<data.roots.length; i++){
			for( var i=data.roots.length-1; i>=0; i--){
				var root = data.roots[i];
				var $el = $('<li data-id="'+ root.id +'">'+ root.title +'</li>');
				$el.on('click',function(ev){	
					ev.preventDefault();
					self.getTask($(this).data('id'));
				});
				$("#task-details .roots").append($el);
			}
			
			//Change details
			var class_type = "task-type-" + data.type;
			$("#task-details").get(0).className = class_type;
			$("#task-details-title").get(0).className = "status_" + data.status;
			$("#task-details-title").html(data.title);
			$("#task-details-status-select").val(data.status || "todo");
			$("#task-details-description").html(data.description);
			$("#task-details-time").html(taskTimeToString(data.time));
			$("#task-details-remaining").html(taskTimeToString(data.remaining));
			$('#task-create [name=parent]').val(data.id);
			$('#task-create [name=type]').val(data.type + 1);
			
			//Change edit form too
			$("#task-details .edit [name=title]").val(data.title);
			$("#task-details .edit [name=description]").val(data.description);
			$("#task-details .edit [name=time]").val(taskTimeToString(data.time));
			$("#task-details .edit [name=type]").val(data.type);
			$("#task-details .edit [name=id]").val(data.id);
			
			$('#task-details').show();
			
			window.setPath(data.id, data.title);
		} else {
			$('#task-details').hide();
			$('#task-create [name=parent]').val(-1);
			$('#task-create [name=type]').val(0);
		}
		$('#sub-tasks ul').html("");

		let _statusVal = function(a){
			if(a == "inprogress") {return 3;}
			if(a == "todo") {return 2;}
			if(a == "pendingreview") {return 1;}
			if(a == "complete") {return 0;}
			return 0;
		}
		data.subtasks.sort(function(a,b){
			//Sort subtasks by status
			if(a.status == b.status){
				return (a.status > b.status) ? 1 : -1;
			} else  {
				return _statusVal(a.status) - _statusVal(b.status);
			}
		});
		if(data.subtasks.length){
			for( var i=0; i<data.subtasks.length; i++){
				var task = data.subtasks[i];
				var class_status = "status_" + task.status;
				var class_type = "task-type-" + task.type;
				var $el = $('<li class="subtask '+class_type+'" data-id="'+ task.id +'">'+
					'<h3 class="'+class_status+'">' + task.title + '</h3>' +
					'<div>' + taskTimeToString( task.remaining ) + '</div>' +
				'</li>');
				$('#sub-tasks ul').append($el);
				$el.on('click',function(ev){
					ev.preventDefault();
					self.getTask( $(this).data('id') );
				});
			}
			$('#sub-tasks').show();
		}else{
			$('#sub-tasks').hide();
		}
		
		$('#task-details .display').show();
		$('#task-details .edit').hide();
		$('#current-task').show();
	},
	
	renderReports : function(data){
		$('#reports ul').html("");
		
		for(var i=0; i< data.tasks.length; i++){
			var task = data.tasks[i];
			var $el = $(''+
				'<li>'+
					'<h3>' + task.title + '</h3>' +
					'<ul></ul>'+
				'</li>'
			);
			for(var j=0; j< task.reports.length; j++){
				var report = task.reports[j];
				$el.children('ul').append(''+
					'<li>'+
						'<p>'+ report.description +'</p>'+
						'<p>by: '+ report.user.name +' - '+ report.created +'</p>'+
					'</li>'
				);
			}
			$('#reports ul').append( $el );
		}
	},
	
	headerLogin : function(){
		var data = { 
			"name" : $('#userPanel .login [name=name]').val(),
			"password" : $('#userPanel .login [name=password]').val()
		};
		
		$.ajax({
			"url" : "/api/user/login",
			"type" : "post",
			"data" : data,
			"success" : function(res){
				if( res.id ) {
					App.trigger("login", res);
				}
			}
		});
	},
	
	createUser : function() {
		var data = {
			"name" : $('#user-create [name=name]').val(),
			"password" : $('#user-create [name=password]').val(),
			"token" : App.userToken
		}
		$.ajax({
			"url" : "/api/user/create",
			"type" : "post",
			"data" : data,
			"success" : function(res){
				App.trigger('user.created',res);
			}
		});
	},
	getTask : function(id){
		this.clearTask();
		
		$.ajax({
			"url" : "/api/task/" + id,
			"success" : function(res){
				App.trigger('task.loaded',res);
			}
		});
	},
	createTask : function() {
		var self = this;
		var data = {"token" : App.userToken};
		$('#task-create [name]').each(function(index){
			data[$(this).attr('name')] = $(this).val();
		});
		
		$.ajax({
			"url" : "/api/task/create",
			"type" : "post",
			"data" : data,
			"success" : function(res){
				self.clearCreateTaskForm();
				self.refreshTask();
			}
		});
	},
	
	editTask : function(){
		var self = this;
		var data = {"token" : App.userToken};
		$('#task-details .edit [name]').each(function(index){
			data[$(this).attr('name')] = $(this).val();
		});
		
		$.ajax({
			"url" : "/api/task/edit/" + data.id,
			"type" : "post",
			"data" : data,
			"success" : function(res){
				App.trigger('task.loaded',res);
			}
		});
	},

	deleteTask : function(id){
		var self = this;
		var data = {token:App.userToken,id:id};
		$.ajax({
			"url" : "/api/task/delete/" + data.id,
			"type" : "post",
			"data" : data,
			"success" : function(res){
				if(res.parent_id){
					self.getTask(res.parent_id);
				}else{
					self.goHome();
				}
			}
		})
	},
	
	updateTaskStatus : function(id, status){
		var data = {"token":App.userToken, "id":id, "status":status};
		$.ajax({
			"url" : "/api/task/status/" + data.id,
			"type" : "post",
			"data" : data,
			"success" : function(res){
				App.trigger('task.loaded',res);
			}
		});
	},
	
	clearCreateTaskForm : function(){
		$('#task-create [name]').each(function(index){
			$(this).val("");
		});
	}
}

function taskTimeToString(t){
	var days = 0;
	var hours = 0;
	var min = 0;
	
	var hour_length = 60;
	var day_length = 8 * hour_length;
	
	if ( t >= day_length ) {
		days = Math.floor(t/day_length);
		hours = Math.floor( (t - (days*day_length)) / hour_length );
	}else if ( t >= hour_length ){
		hours = Math.floor(t/hour_length);
		min = Math.floor( t - (hours*hour_length) );
	} else {
		min = t;
	}
	
	var out = "" +
		(days > 0 ? days + "d " : "") +
		(hours > 0 ? hours + "h " : "") +
		(min > 0 ? min + "m" : "");
	return out.trim();
}

window.setPath = function(path, name){
	if( path[0] != "/" ) {
		path = "/" + path;
	}
	if( path != location.pathname ) {
		history.pushState(name, "", path);
	}
}